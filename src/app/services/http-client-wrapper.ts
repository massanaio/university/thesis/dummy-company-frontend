import { Headers, RequestOptions, Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';


@Injectable()
export class HttpClientWrapper {
  private baseURL: String;
  private headers: Headers;
  private options: RequestOptions;
  public pendingRequests = 0;

  private optionsWithoutAuth: RequestOptions;

  constructor(
    private http: Http
  ) {

    this.baseURL = environment.SERVER_URL;
    this.headers = new Headers({ 'Content-Type': 'application/json' });

    this.optionsWithoutAuth = new RequestOptions({ headers: new Headers({ 'Content-Type': 'application/json' }) });
    this.setRequestOptions();

  }  

  setRequestOptions() {
    this.options = new RequestOptions({ headers: this.headers });
  }

  get(url: string): Observable<any> {
    this.pendingRequests++;
    return this.http.get(this.baseURL +  url, this.options);
  }

  post(url: string, data?: any): Observable<any> {
    return this.http.post(this.baseURL  + url, data, this.options);
  }

}
