import { Injectable } from '@angular/core';
import { VerificationDocumentsDTO } from '../dto/verification-documents-dto';
import { HttpClientWrapper } from './http-client-wrapper';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { VerificationIdentifierDTO } from '../dto/verification-identifier-dto';
import { UserDTO } from '../dto/user-dto';

@Injectable({
  providedIn: 'root'
})
export class VerificationService {

  constructor(private httpClientWrapper: HttpClientWrapper) { }

  verifyThroughDocuments(verificationDocuments: VerificationDocumentsDTO): Observable<UserDTO> {
    return this.httpClientWrapper.post(`/identityVerification/documents`, verificationDocuments).pipe(map(response => response.json() || {}));
  }

  verifyThroughIdentifier(verificationIdentifier: VerificationIdentifierDTO): Observable<UserDTO> {
    return this.httpClientWrapper.post(`/identityVerification/identifier`, verificationIdentifier).pipe(map(response => response.json() || {}));
  }

}
