import { Injectable } from '@angular/core';
import { HttpClientWrapper } from './http-client-wrapper';
import { UserCreateDTO } from '../dto/user-create-dto';
import { Observable } from 'rxjs';
import { UserDTO } from '../dto/user-dto';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClientWrapper: HttpClientWrapper) { }

  createUser(user: UserCreateDTO): Observable<UserDTO> {
    return this.httpClientWrapper.post(`/users`, user).pipe(map(response => response.json() || {}));
  }

  getUsers(): Observable<UserDTO[]> {
    return this.httpClientWrapper.get(`/users`).pipe(map(response => response.json() || {}));
  }

  getUserByDocumentNumber(documentNumber: string): Observable<UserDTO> {
    return this.httpClientWrapper.get(`/users/${documentNumber}`).pipe(map(response => response.json() || {}));
  }

}
