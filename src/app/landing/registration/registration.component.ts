import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { UserCreateDTO } from 'src/app/dto/user-create-dto';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  signupForm: FormGroup;
  registered = false;

  constructor(
    private userService: UserService
  ) { }

  ngOnInit() {
    this.createForm();
  }

  public createForm(){
    this.signupForm = new FormGroup({
      name: new FormControl(''),
      documentNumber: new FormControl(''),
      address: new FormControl('')
    });
  }

  public createUser(){  
    console.log("Creating user");

    let name = this.signupForm.get('name').value;
    let documentNumber = this.signupForm.get('documentNumber').value;
    let address = this.signupForm.get('address').value;

    let user = new UserCreateDTO(name, documentNumber, address);

    this.userService.createUser(user).subscribe(
      response => {
        this.registered = true;
      },
      error => {
      }
    );
  }

  public fillFormRandomValues(){
    console.log("Fill Form!");

    this.signupForm.controls['name'].setValue(this.getRandomName());
    this.signupForm.controls['documentNumber'].setValue(this.getRandomDocumentNumber());
    this.signupForm.controls['address'].setValue(this.getRandomAddress());

  }

  private getRandomDocumentNumber(): string{
    let documentNumber = '';

    let numbers = "0123456789"
    let chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

    for (var i=0; i < 8; i++) {
      var randomNumber = Math.floor(Math.random() * numbers.length);
      documentNumber += numbers.substring(randomNumber, randomNumber + 1);
    }

    var randomCharacter = Math.floor(Math.random() * chars.length);
    documentNumber += chars.substring(randomCharacter, randomCharacter + 1);

    return documentNumber;
  }  

  private getRandomName(){
    let names = ['Juan','Laura','Lorena','David','Marc','Carla','Helen','Olivia','Tom', 'Oliver', 'Andrea', 'Javier', 'Miquel', 'Victor', 'Xavier', 'Ricard', 'Rachel', 'Miranda', 'Diane'];
    let surnames = ['Smith','Jones','Taylor','Brown','Williams','Muller','Schmidt','Schneider','Fischer', 'Rossi', 'Bianchi', 'Ricci', 'Pique', 'Suarez', 'Alba', 'Puyol', 'Bale', 'Hanks','Hamilton','Senna','Clark','Mansell'];    

    var randomName = Math.floor(Math.random() * names.length);
    var randomSurname = Math.floor(Math.random() * surnames.length);

    return names[randomName] + ' ' + surnames[randomSurname];    
  }

  private getRandomAddress(){
    let types = ['Carrer','Avinguda','Plaça','Camí','Carretera','Passeig', 'Via'];
    let streetNames = ['Diagonal','Gaudi','Barcelona','Mar','Reial','Major','Tallers','Laietana','Catalunya'];    
    let cities = ['Barcelona','New York','Paris','Berlin','Tokyo','Roma','Atenes','Dublin','London', 'San Francisco', 'Rio de Janeiro', 'Hong Kong'];  
    let countries = ['USA','UK','Italia','France','Deutschland','Russia','China','Israel','Singapur', 'Argentina', 'Canada', 'Portugal'];  

    var randomType = Math.floor(Math.random() * types.length);
    var randomName = Math.floor(Math.random() * streetNames.length);
    var randomCity = Math.floor(Math.random() * cities.length);
    var randomCountry = Math.floor(Math.random() * countries.length);

    return types[randomType] + ' ' + streetNames[randomName] + ', ' + cities[randomCity] + ', ' + countries[randomCountry];    
  }

}
