export enum VerificationStatus {
    PENDING = <any>"PENDING",
    IN_PROCESS = <any>"IN_PROCESS",
    VERIFIED = <any>"VERIFIED"
}