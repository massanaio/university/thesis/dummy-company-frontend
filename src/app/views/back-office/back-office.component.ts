import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { VerificationStatus } from 'src/app/enums/verification-status.enum';
import { UserDTO } from 'src/app/dto/user-dto';
import { timer } from 'rxjs';

@Component({
  selector: 'app-back-office',
  templateUrl: './back-office.component.html',
  styleUrls: ['./back-office.component.css']
})
export class BackOfficeComponent implements OnInit {

  public users: UserDTO[] = [];

  public pending: UserDTO[] = [];
  public in_process: UserDTO[] = [];
  public verified: UserDTO[] = [];

  public timer = timer(2000, 2000);

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getUsers();
    this.timer.subscribe(() => {
      this.getUsers();
    })
  }

  private getUsers() {
    this.userService.getUsers().subscribe(
      response => {
        this.users = response;
        this.filterByStatus();
      },
      error => {
        console.log(error);
      }
    )
  }

  private filterByStatus() {
    this.pending = this.users.filter(
      user => user.verificationStatus === VerificationStatus.PENDING);
    this.in_process = this.users.filter(
      user => user.verificationStatus === VerificationStatus.IN_PROCESS);
    this.verified = this.users.filter(
      user => user.verificationStatus === VerificationStatus.VERIFIED);
  }


}
