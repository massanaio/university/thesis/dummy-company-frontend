import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserAreaComponent } from './user-area/user-area.component';
import { BackOfficeComponent } from './back-office/back-office.component';

const routes: Routes = [
  {
    path: 'user-area',
    component: UserAreaComponent
  },
  {
    path: 'user-area/:documentNumber',
    component: UserAreaComponent
  },
  {
    path: 'back-office',
    component: BackOfficeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewsRoutingModule { }
