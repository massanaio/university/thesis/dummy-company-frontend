import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ViewsRoutingModule } from './views-routing.module';
import { UserAreaComponent } from './user-area/user-area.component';
import { BackOfficeComponent } from './back-office/back-office.component';

@NgModule({
  declarations: [UserAreaComponent, BackOfficeComponent],
  imports: [
    CommonModule,
    ViewsRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ViewsModule { }
