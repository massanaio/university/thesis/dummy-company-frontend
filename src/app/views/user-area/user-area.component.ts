import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { UserDTO } from 'src/app/dto/user-dto';
import { FormGroup, FormControl } from '@angular/forms';
import { VerificationService } from 'src/app/services/verification.service';
import { VerificationDocumentsDTO } from 'src/app/dto/verification-documents-dto';
import { VerificationIdentifierDTO } from 'src/app/dto/verification-identifier-dto';
import { timer } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-user-area',
  templateUrl: './user-area.component.html',
  styleUrls: ['./user-area.component.css']
})
export class UserAreaComponent implements OnInit {

  public users: UserDTO[];

  public selectedDocumentNumber: string;
  
  public identifierForm: FormGroup;

  public selectedUser: UserDTO;
  public documentNumber: string;

  public timer = timer(5000, 5000);

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private router: Router,
    private verificationService: VerificationService
  ) {
    this.route.params.subscribe(params => {
      this.documentNumber = params['documentNumber'];
      this.getUser();
    })
  }

  ngOnInit() {
    this.getAllUsers();
    this.createForm();
    this.timer.subscribe(() => {
      this.getUser();
    })
  }

  public verifyDocuments() {
    let name = this.selectedUser.name;
    let documentNumber = this.selectedUser.documentNumber;
    let address = this.selectedUser.address;

    let verificationDocumentsDTO = new VerificationDocumentsDTO(name, documentNumber, address);

    this.verificationService.verifyThroughDocuments(verificationDocumentsDTO).subscribe(
      response => {
        this.getAllUsers();
        this.selectedUser = response;
      },
      error => {
        console.log(error);
      }
    )
  }

  selectUser() {
    console.log(this.selectedDocumentNumber);
    this.router.navigate([`/user-area/${this.selectedDocumentNumber}`]);
  }

  public verifyIdentifier() {
    let documentNumber = this.selectedUser.documentNumber;
    let userIdentifier = this.identifierForm.get('identifier').value;

    let verificationIdentifierDTO = new VerificationIdentifierDTO(documentNumber, userIdentifier);

    this.verificationService.verifyThroughIdentifier(verificationIdentifierDTO).subscribe(
      response => {
        this.getAllUsers();
        this.selectedUser = response;
      },
      error => {
        console.log(error);
      }
    )
  }

  private getAllUsers() {
    this.userService.getUsers().subscribe(
      response => {
        this.users = response;
        console.log(this.users);
      },
      error => {
        console.log(error);
      }
    )
  }

  private getUser() {
    this.userService.getUserByDocumentNumber(this.documentNumber).subscribe(
      response => {
        this.selectedUser = response;
        console.log(this.selectedUser);
      },
      error => {
        console.log(error);
      }
    )
  }

  public createForm() {
    this.identifierForm = new FormGroup({
      identifier: new FormControl('')
    });
  }

}
