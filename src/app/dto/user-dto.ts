import { VerificationStatus } from '../enums/verification-status.enum';

export class UserDTO {
    constructor(
        public name: string,
        public documentNumber: string,
        public address: string,
        public verificationStatus: VerificationStatus,
        public blockchainIdentifier: string
    ){}
}