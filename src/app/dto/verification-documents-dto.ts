export class VerificationDocumentsDTO {
    constructor(
        public name: string,
        public documentNumber: string,
        public address: string
    ){}
}