export class UserCreateDTO {
    constructor(
        public name: string,
        public documentNumber: string,
        public address: string
    ){}
}