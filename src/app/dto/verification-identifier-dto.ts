export class VerificationIdentifierDTO {
    constructor(
        public documentNumber: string,
        public userIdentifier: string
    ){}
}